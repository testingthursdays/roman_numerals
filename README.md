ROMAN NUMERALS
==============

Write a program to convert arabic numerals to Roman Numerals and then write another program to convert Roman Numerals into arabic numerals. 

Definitions
-----------

Roman numeral only represent natural numbers. There is no concept o zero or negative numbers.

The numbers are formed by combining symbols and adding the following values:

- I = 1
- V = 5
- X = 10
- L = 50
- C = 100
- D = 500
- M = 1000

Symbols are placed from left to right in order value, starting from the largest. However, to avoid 4 characters being repeated in sucession, subtractive notation is used. Legal sequences are as follows:

- IV = 4
- IX = 9
- XL = 40
- XC = 90
- CD = 400
- CM = 900

Examples
--------

- 3 = III
- 6 = VI
- 12 = XII
- 15 = XV
- 19 = XIX
- 35 = XXXV
- 99 = LXLIX
- 111 = CXI
- 1954 = MCMLIV
- 1990 = MCMXC
- 2014 = MMXIV